set -x

BUILD_DOCKER=true
RESETUJ_VOLUMES=false
KREIRAJ_DIREKTORIJUME=true
INICIJALIZUJ=true
STARTUJ=true
PRERENDER=false

POSALJI_IMAGE_NA_SERVER=false
IMAGE_VERZIJA=0.18

#DRZAVE=serbia,montenegro,kosovo,croatia,bosnia-herzegovina,slovenia,macedonia
#OKOLNE_DRZAVE=albania,bulgaria,greece,italy,romania,hungary
DRZAVE=serbia,kosovo
OKOLNE_DRZAVE=

#DOCKER_KOMANDA='docker '
DOCKER_KOMANDA='sudo docker '

DOCKER=pmilanovic/openstreetmap-tile-server-serbian
# DOCKER=mojtileserver:0.x

RESTART=' --restart always '
# RESTART=' '

#DATA_VOLUME=osm-data
#TILES_VOLUME=osm-rendered-tiles
DATA_VOLUME=/home/$USER/osm-data
TILES_VOLUME=/home/$USER/osm-rendered-tiles

UPDATES=' -e UPDATES=enabled '
#UPDATES='  '

LOG=' -v openstreetmap-apache-log:/var/log/apache2'
#LOG=' -v /home/mpele/logApache:/var/log/apache2'

DEBUG_JOSM_transliterator=' '
#DEBUG_JOSM_transliterator=' -v /home/mpele/git/JOSM_transliterator/bin/:/jbin'


##################################################

$DOCKER_KOMANDA container prune -f

if [ "$RESETUJ_VOLUMES" = true ]; then
    echo ' Resetuje docker volumes'
    $DOCKER_KOMANDA volume rm $DATA_VOLUME
    $DOCKER_KOMANDA volume rm $TILES_VOLUME

    set -e

    $DOCKER_KOMANDA volume create $DATA_VOLUME
    $DOCKER_KOMANDA volume create $TILES_VOLUME
fi


if [ "$KREIRAJ_DIREKTORIJUME" = true ]; then
    echo '   Kreira direktorijume'
    sudo mkdir $DATA_VOLUME
    mkdir $TILES_VOLUME
fi


set -e

if [ "$BUILD_DOCKER" = true ]; then
   $DOCKER_KOMANDA build -t mojtileserver:0.x .
fi


if [ "$INICIJALIZUJ" = true ]; then
   echo '   Startuje inicijalizaciju baze'
   $DOCKER_KOMANDA run \
      -e DRZAVE=$DRZAVE \
      -e OKOLNE_DRZAVE=$OKOLNE_DRZAVE \
      $UPDATES  \
      -v $DATA_VOLUME:/data/database/ \
      -v $TILES_VOLUME:/data/tiles/ \
      $DOCKER \
      priprema_rendera
fi

$DOCKER_KOMANDA container prune -f

if [ "$STARTUJ" = true ]; then
   $DOCKER_KOMANDA run \
      -p 8080:80 \
      --shm-size="192m" \
      $RESTART \
      $UPDATES \
      -e PODESAVANJA="debugUpdate=true" \
      -v $DATA_VOLUME:/data/database/ \
      -v $TILES_VOLUME:/data/tiles/ \
      $LOG \
      $DEBUG_JOSM_transliterator \
     -d $DOCKER \
     run
fi


if [ "$PRERENDER" = true ]; then
     c=$($DOCKER_KOMANDA ps | grep "Up" | grep -Eo "^[0-9a-f]+")
     $DOCKER_KOMANDA exec -u renderer "$c" bash -c '/home/renderer/render_list_geo.pl/render_list_geo.pl -n 2 -l 80 -f -a -m cir -z 2 -Z 15 $(cat /data/database/boundingBox.txt)'
     $DOCKER_KOMANDA exec -u renderer "$c" bash -c '/home/renderer/render_list_geo.pl/render_list_geo.pl -n 2 -l 80 -f -a -m lat -z 2 -Z 15 $(cat /data/database/boundingBox.txt)'
fi


if [ "$POSALJI_IMAGE_NA_SERVER" = true ]; then
    $DOCKER_KOMANDA tag mojtileserver:0.x pmilanovic/openstreetmap-tile-server-serbian:$IMAGE_VERZIJA
    $DOCKER_KOMANDA push pmilanovic/openstreetmap-tile-server-serbian:$IMAGE_VERZIJA

    $DOCKER_KOMANDA tag mojtileserver:0.x pmilanovic/openstreetmap-tile-server-serbian:latest
    $DOCKER_KOMANDA push pmilanovic/openstreetmap-tile-server-serbian:latest
fi
