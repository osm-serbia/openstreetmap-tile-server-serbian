#!/bin/bash

cd

sudo -u renderer psql -X -A -d gis -t -c "drop table rgz_adrese"
wget https://openstreetmap.rs/download/rgz_adrese_dump_latest.zip
unzip rgz_adrese_dump_latest.zip
shp2pgsql -I -s 3857 -d rgz_adrese.shp rgz_adrese | psql -d gis -U renderer
rm -v !(*.sh)

sudo -u renderer psql -X -A -d gis -t -c "drop table rgz_ulice"
wget https://openstreetmap.rs/download/rgz_ulice_dump_latest.zip
unzip rgz_ulice_dump_latest.zip
shp2pgsql -I -s 3857 -d rgz_ulice.shp  rgz_ulice | psql -d gis -U renderer
rm -v !(*.sh)

rm -R /data/tiles/rgz/
