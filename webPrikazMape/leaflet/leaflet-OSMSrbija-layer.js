L.TileLayer.OSMSrbija = L.TileLayer.extend({

    getTileUrl: function (coords) {

        var layerBounds = new L.LatLngBounds (new L.LatLng (47.14, 11.15), new L.LatLng (40.88, 25.41));

        var coordBounds = this._tileCoordsToBounds(coords);

        if (coords.z >= 6 && layerBounds.contains (coordBounds)) {
          return "/cir/" + coords.z + "/" + coords.x + "/" + coords.y + ".png";
        } else {
          return "https://c.tile.openstreetmap.org/" + coords.z + "/" + coords.x + "/" + coords.y + ".png";
        }
    },

    getAttribution: function() {
        return "<a href='https://openstreetmap.rs'>OpenStreetMap Serbia</a>";
    }

});

L.tileLayer.OSMSrbija = function() {
    return new L.TileLayer.OSMSrbija();
}
