import sys
import math

def num2deg(xtile, ytile, zoom):
  n = 2.0 ** zoom
  lon_deg = (xtile+0.5) / n * 360.0 - 180.0
  lat_rad = math.atan(math.sinh(math.pi * (1 - 2 * (ytile+0.5) / n)))
  lat_deg = math.degrees(lat_rad)
  return (lat_deg, lon_deg)

print('var podaci_'+ sys.argv[2] + '_' +sys.argv[1] +' = [')

for line in sys.stdin:
    podeljeno=line.split(sep="/")
    if len(podeljeno) > 3:
        [lat,lon]=num2deg(int(podeljeno[3]), int(podeljeno[4].split(sep=".")[0]), int(podeljeno[2]))
#        print(line[:-1], podeljeno[2], podeljeno[3], podeljeno[4].split(sep=".")[0], num2deg(int(podeljeno[4].split(sep=".")[0]), int(podeljeno[3]), int(podeljeno[2])))
        print('[{},{},{}],'.format(lat, lon, int(podeljeno[0])))

print('];')
print(' ')
